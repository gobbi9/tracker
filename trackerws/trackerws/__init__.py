from flask import Flask
from flask_socketio import SocketIO

appws = Flask(__name__)
appws.secret_key = '\xf9\xe7c\x84\x80FP\x049\x1442\x19\xb6\xdf\x9aE\xc5\n\xfa\xc4\xffO\xef'
appws.config['JSON_ADD_STATUS'] = True
appws.config['JSON_USE_ENCODE_METHODS'] = True
socketio = SocketIO(appws)

if __name__ == '__main__':
	socketio.run(appws)
	
import trackerws.views
