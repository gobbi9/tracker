from trackerws import appws, socketio
'''
from flask_json import FlaskJSON, JsonError, json_response, as_json
from tracker.database import db_session, init_db
from faker import Factory
from dateutil.relativedelta import relativedelta
from flask import url_for, session
import os
from flask import send_from_directory, copy_current_request_context, request, render_template
from flask_bcrypt import Bcrypt
from datetime import datetime
from tracker.data.models import Coordinate, Client
from sqlalchemy import and_, desc
'''
from flask_socketio import send, emit

if not appws.debug:
	from raven.contrib.flask import Sentry
	sentry = Sentry(appws, dsn='https://28992cd5691a4ade95147c15e2c87274:8d6862eb2b764ddbaf02b3aeecc7d4b6@sentry.io/138302')	

	import logging
	from logging import Formatter
	from logging.handlers import TimedRotatingFileHandler
	file_handler = TimedRotatingFileHandler('log.txt', when="midnight")
	file_handler.setLevel(logging.WARNING)
	file_handler.setFormatter(Formatter(
    	'%(asctime)s %(levelname)s: %(message)s '
    	'[in %(pathname)s:%(lineno)d]'
	))
	appws.logger.addHandler(file_handler)

def ack(arg1):
    print('pong was received!', arg1)

@socketio.on('ping')
def handle_message(message):
	print('received message: ' + str(message))
	emit('pong', '{"response": "pongpong"}', callback=ack)
	return "pong1"