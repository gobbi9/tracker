from setuptools import setup, find_packages

setup(
    name='trackerws',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask==0.12',
        'flask-socketio',
        'raven==5.32.0',
        'blinker==1.4',
        'eventlet',
        'gunicorn'
    ]
)