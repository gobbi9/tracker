BEGIN;
insert into client(login, email, password, member_since)
	values('arduino', 'gobbi9@live.com', '$2b$12$8GJRaREFGRz4gwUrOnz3xeeYnc3u3SXEgkKhsokJ1VWwXKYp0gdHO', '2017-01-07');

insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-900000000', '1771882380', '2017-01-09 21:31:15.811300');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-900000000', '1293757490', '2017-01-09 21:31:15.811313');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-891154290', '1237501070', '2017-01-09 21:31:15.811317');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-877697750', '1653442410', '2017-01-09 21:31:15.811319');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-871875000', '1589069750', '2017-01-09 21:31:15.811322');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-871875000', '1237507350', '2017-01-09 21:31:15.811324');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-843750000', '1350007780', '2017-01-09 21:31:15.811326');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-843750000', '1153132660', '2017-01-09 21:31:15.811328');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-843750000', '1040632590', '2017-01-09 21:31:15.811330');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-843750000', '1012507800', '2017-01-09 21:31:15.811332');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-843750000', '1012507340', '2017-01-09 21:31:15.811334');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-824414060', '1082922390', '2017-01-09 21:31:15.811336');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-815625000', '1575007660', '2017-01-09 21:31:15.811338');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-805627440', '1579614290', '2017-01-09 21:31:15.811339');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-805517580', '1579284700', '2017-01-09 21:31:15.811341');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-805517580', '1578845240', '2017-01-09 21:31:15.811343');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-804473020', '1373773550', '2017-01-09 21:31:15.811345');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-787500000', '1237507380', '2017-01-09 21:31:15.811347');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-787500000', '1181257510', '2017-01-09 21:31:15.811349');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-787500000', '1096882780', '2017-01-09 21:31:15.811351');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-787499970', '1096882780', '2017-01-09 21:31:15.811353');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-778591613', '1666901550', '2017-01-09 21:31:15.811355');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-778591613', '1666898345', '2017-01-09 21:31:15.811357');
insert into coordinate(login, latitude, longitude, time) 
 values ('arduino', '-778591384', '1666911621', '2017-01-09 21:31:15.811359');

COMMIT;
