BEGIN;

alter table coordinate add column serial_number bigint;

alter table coordinate modify column serial_number bigint primary key auto_increment;
COMMIT;