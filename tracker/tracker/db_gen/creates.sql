BEGIN;

create table client(
	login varchar(20) primary key,
	email varchar(50) unique not null,
	password char(60) not null,
	member_since date
);

create table coordinate(
	latitude varchar(10) not null,
	longitude varchar(10) not null,
	time timestamp(6) unique not null,
	login varchar(20) not null,
	serial_number integer primary key,
	foreign key(login) references client(login)
);

COMMIT;
