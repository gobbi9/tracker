from sqlalchemy import create_engine, exc, event, select
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from tracker import app
from urllib.parse import quote_plus as urlquote
from flask_migrate import Migrate, upgrade
import os

HOST = os.environ.get('TRACKER_MYSQL_HOST')
PORT = os.environ.get('TRACKER_MYSQL_INT_PORT')
PWD = os.environ.get('TRACKER_MYSQL_PWD')

if HOST == None:
    HOST = "localhost"
if PORT == None:
    PORT = "3306"
if PWD == None:
    PWD = "+BTkAS}_5@LY)Y"    

db_url = 'mysql+cymysql://root:{}@{}:{}/tracker'.format(urlquote(PWD), HOST, PORT)

app.config['SQLALCHEMY_DATABASE_URI'] = db_url
    
engine = create_engine(db_url, convert_unicode=True, pool_recycle=3600)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         expire_on_commit = False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

migrate = Migrate(app, Base)

# import all modules here that might define models so that
# they will be registered properly on the metadata.  Otherwise
# you will have to import them first before calling init_db()
def init_db():    
    # auto upgrade on startup https://flask-migrate.readthedocs.io/en/latest/
    with app.app_context():
        upgrade()
    import tracker.data.models
    Base.metadata.create_all(bind=engine)
    
@event.listens_for(engine, "engine_connect")
def ping_connection(connection, branch):
    if branch:
        # "branch" refers to a sub-connection of a connection,
        # we don't want to bother pinging on these.
        return

    # turn off "close with result".  This flag is only used with
    # "connectionless" execution, otherwise will be False in any case
    save_should_close_with_result = connection.should_close_with_result
    connection.should_close_with_result = False

    try:
        # run a SELECT 1.   use a core select() so that
        # the SELECT of a scalar value without a table is
        # appropriately formatted for the backend
        connection.scalar(select([1]))
    except exc.DBAPIError as err:
        # catch SQLAlchemy's DBAPIError, which is a wrapper
        # for the DBAPI's exception.  It includes a .connection_invalidated
        # attribute which specifies if this connection is a "disconnect"
        # condition, which is based on inspection of the original exception
        # by the dialect in use.
        if err.connection_invalidated:
            # run the same SELECT again - the connection will re-validate
            # itself and establish a new connection.  The disconnect detection
            # here also causes the whole connection pool to be invalidated
            # so that all stale connections are discarded.
            connection.scalar(select([1]))
        else:
            raise
    finally:
        # restore "close with result"
        connection.should_close_with_result = save_should_close_with_result