from flask import Flask

app = Flask(__name__)
app.secret_key = '\xf9\xe7c\x84\x80FP\x049\x1442\x19\xb6\xdf\x9aE\xc5\n\xfa\xc4\xffO\xef'
app.config['JSON_ADD_STATUS'] = True
app.config['JSON_USE_ENCODE_METHODS'] = True

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response

import tracker.views
