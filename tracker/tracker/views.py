from tracker import app
from flask_json import FlaskJSON, JsonError, json_response, as_json
from tracker.database import db_session, init_db
from faker import Factory
from dateutil.relativedelta import relativedelta
from flask import url_for, session, request
import os
from flask import send_from_directory, copy_current_request_context, request, render_template
from flask_bcrypt import Bcrypt
from datetime import datetime, timedelta
from tracker.data.models import Coordinate, Sender, Receiver
from sqlalchemy import and_, desc, asc
from random import randint
from math import pi, cos
#from tracker.model.coordinate import Coordinate

FlaskJSON(app)
fbcrypt = Bcrypt(app)
fakerFactory = Factory.create()
init_db()

lastCoordinate = Coordinate(str(fakerFactory.latitude()), str(fakerFactory.longitude()), datetime.utcnow())
timeMap = {}
#indicates if the android client is ready
readyMap = {}

#### maps for new methods
ready = {}
time = {}

if not app.debug:
	from raven.contrib.flask import Sentry
	sentry = Sentry(app, dsn='https://c50fbdbfd0f645c1b1a756ed041c7837:790e5c7dcaf048868b42760ae9150cf9@sentry.io/125517')

	import logging
	from logging import Formatter
	from logging.handlers import TimedRotatingFileHandler
	file_handler = TimedRotatingFileHandler('log.txt', when="midnight")
	file_handler.setLevel(logging.WARNING)
	file_handler.setFormatter(Formatter(
    	'%(asctime)s %(levelname)s: %(message)s '
    	'[in %(pathname)s:%(lineno)d]'
	))
	app.logger.addHandler(file_handler)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join('.', 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

'''
Arduino
GET http://tracker.gobbi.info/checkConnection?login=arduino&password=jk+eUxwJ%5E%24aaDq2%5B
'''
@app.route('/checkConnection')
def checkConnection():
	global readyMap, timeMap

	login = request.args.get('login')
	password = request.args.get('password')

	_sender = Sender(login = login, password = password)

	sender = Sender.query.filter(Sender.login == _sender.login).first()
	userExists = sender != None

	if userExists and fbcrypt.check_password_hash(sender.password, _sender.password):
		if sender.login in readyMap and readyMap[sender.login]:
			timeMap[login] = datetime.utcnow()
			return "OK"

	return "ERROR"

# GET http://tracker.gobbi.info/add?l=arduino&p=jk+eUxwJ%5E%24aaDq2%5B&lat=5320.12345&lng=5320.12345&t=20170127223431
"""
Arduino
The raw gps information from the serial is in the form: DDmm.mmmmm. e.g.If the raw information is: 5320.12345, then it is: 53 degress and 20.12345 minutes. In order to convert it to decimal coordinates you divide the minutes by 60. i.e. 20.12345 / 60 = .33539 Finally, the decimal result: 53.33539
"""
@app.route('/add')
def addCoordinate():
	global readyMap

	# database upto4digits6digits

	"""
	login = request.args.get('login')
	password = request.args.get('password')
	latitude = request.args.get('latitude')
	longitude = request.args.get('longitude')
	time = request.args.get('time')
	"""
	login = request.args.get('l')
	password = request.args.get('p')
	latitude = request.args.get('lat')
	longitude = request.args.get('lng')
	captured_at = request.args.get('t')

	try:
		_sender = Sender(login = login, password = password)
		coordinate = Coordinate(latitude = arduinoToGoogleCoordinate(latitude), longitude = arduinoToGoogleCoordinate(longitude),\
			captured_at = datetime.strptime(captured_at, "%Y%m%d%H%M%S"), received_at = datetime.utcnow(), login = _sender.login) #yyyymmddHHMMSS

		sender = Sender.query.filter(Sender.login == _sender.login).first()
		userExists = sender != None

		if userExists and fbcrypt.check_password_hash(sender.password, _sender.password):
			if sender.login in readyMap and readyMap[sender.login]:
				db_session.add(coordinate)
				db_session.commit()
				db_session.flush()				
				#return json_response(200, info = "Coordinate added.")
				return "OK " + str(coordinate.serial_number)
			else:
				return "ERROR"

	except (KeyError, TypeError, ValueError):
		#raise JsonError(description='Invalid value.')
		#return json_response(400, info = "Something went wrong.")
		return "ERROR"

	return "ERROR"
	#return json_response(400, info = "Something went wrong.")

def toStrRemovePoint(coord):
	return "{0:.6f}".format(coord).replace(".","")

@app.route('/start/<login>')
def start_tracking(login):
	global timeMap

	sender = Sender.query.filter(Sender.login == login).first()
	userExists = sender != None

	if userExists:
		timeMap[login] = datetime.utcnow()
		#return json_response(200, info="Tracking started...")
		return "OK"
	else:
		return "ERROR"
		#return json_response(400, info="Tracking could not be started for user {}.".format(login))


#Android
'''
Android/Web:
GET https://tracker.gobbi.info/readyToReceive/arduino
GET https://tracker.gobbi.info/stopReceiving/arduino
'''
@app.route('/readyToReceive/<login>')
def startArduino(login):
	global readyMap

	sender = Sender.query.filter(Sender.login == login).first()
	userExists = sender != None

	if userExists:
		readyMap[login] = True
		return  json_response(200)

	return json_response(400)


'''
Android/Web:
'''
@app.route('/stopReceiving/<login>')
def stopArduino(login):
	global readyMap

	sender = Sender.query.filter(Sender.login == login).first()
	userExists = sender != None

	if userExists:
		readyMap[login] = False
		return  json_response(200)

	return json_response(400)


'''
Android/Web:
'''
@app.route('/position/<login>')
def getCoordinates(login):
	global timeMap

	if login in timeMap:
		t0 = timeMap[login]
		now = datetime.utcnow()
		coordinates = Coordinate.query.filter(and_(Coordinate.login == login, t0 < Coordinate.captured_at, Coordinate.captured_at <= now)).all()
		#coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.captured_at <= now))

		if coordinates:
			timeMap[login] = now

		return json_response(200, coordinates = coordinates)
	else:
		return json_response(400)

def arduinoToGoogleCoordinate(coord):
	isNegative = float(coord) < 0
	signal = 1
	if isNegative:
		signal = -1
		coord = coord[1:]
	degrees = abs(float(coord[0:2]))
	minutes = abs(float(coord[2:]))
	google_coord = (degrees + minutes / 60.0) * signal
	return toStrRemovePoint(google_coord)

'''
Android/Web:
'''
@app.route('/simulate/<login>')
@as_json
def simulate(login):
	r_earth = 6378 * 1000.0 # meters

	sender = Sender.query.filter(Sender.login == login).first()
	userExists = sender != None

	newestCoordinate = Coordinate.query.filter(Coordinate.login == login).order_by(desc(Coordinate.captured_at)).first()
	#topSerial = Coordinate.query.filter(Coordinate.login == login).order_by(desc(Coordinate.serial_number)).first().serial_number
	#topSerial += 1;
	now = datetime.utcnow()

	if newestCoordinate != None and newestCoordinate.captured_at > now:
		t0 = newestCoordinate.captured_at + timedelta(milliseconds=0.001)
	else:
		t0 = now

	lat0 = -22.858618
	long0 = -43.231973

	coordinates = []

	countCoordinates = request.args.get('count')
	try:
		countCoordinates = int(countCoordinates)
	except:
		countCoordinates = 10

	interval = request.args.get('interval')
	try:
		interval = int(interval)
	except:
		interval = 2

	for i in range(countCoordinates):
		coordinate = Coordinate(toStrRemovePoint(lat0), toStrRemovePoint(long0), t0, None, login)
		dx = randint(5,25) # 5 - 25 meters
		dy = randint(5,25)
		lat0  = lat0  + (dy / r_earth) * (180 / pi);
		long0 = long0 + (dx / r_earth) * (180 / pi) / cos(lat0 * pi/180);
		t0 = t0 + timedelta(seconds=interval)
		#topSerial += 1;
		coordinates += [coordinate];
		# add 2 seconds to each date
		# add deltaX to lat0 and deltaY to long0

	'''
	for coordinate in coordinates:
		app.logger.info("{0:.6f}, {1:.6f}".format(float(coordinate.latitude) / 1000000.0, float(coordinate.longitude) / 1000000.0))
	'''

	for coordinate in coordinates:
		db_session.add(coordinate)

	db_session.commit()

	return json_response(200, info = "Track simulation started.")

'''
Android/Web:
'''
@app.route('/history/<login>')
def allCoordinates(login):
	start = request.args.get('start')
	end = request.args.get('end')

	if start == None and end == None:
		coordinates = Coordinate.query.filter(Coordinate.login == login).order_by(desc(Coordinate.captured_at))
	elif start != None and end != None:
		start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%S.%f")
		end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%S.%f")

		if start > end:
			return json_response(400, info = "Start time is bigger than the end time.")

		coordinates = Coordinate.query.filter(\
			and_(Coordinate.login == login, Coordinate.captured_at >= start, Coordinate.captured_at <= end))\
			.order_by(desc(Coordinate.captured_at))
	else:
		if end == None:
			start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%S.%f")
			coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.captured_at >= start)).order_by(desc(Coordinate.captured_at))
		elif start == None:
			end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%S.%f")
			coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.captured_at <= end)).order_by(desc(Coordinate.captured_at))

	return json_response(200, coordinates = coordinates)


#/findBySerial/arduino?serial=20
#/findBySerial/arduino?serial=20&serial=10
#/findBySerial/arduino?start=1&end=10
#/findBySerial/arduino?end=10
#/findBySerial/arduino?start=10
'''
Android/Web:
'''
@app.route('/findBySerial/<login>')
def findBySerial(login):
	serials = request.args.getlist('serial')
	start = request.args.get('start')
	end = request.args.get('end')
	coordinates = []
	if serials and serials != None:
		coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.serial_number.in_(serials)))\
		.order_by(desc(Coordinate.serial_number))
	elif start != None or end != None:
		if start != None and end != None:
			coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.serial_number >= int(start), Coordinate.serial_number <= int(end))).order_by(asc(Coordinate.serial_number))
		elif start == None:
			coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.serial_number <= int(end))).order_by(asc(Coordinate.serial_number))
		elif end == None:
			coordinates = Coordinate.query.filter(and_(Coordinate.login == login, Coordinate.serial_number >= int(start))).order_by(asc(Coordinate.serial_number))
	return json_response(200, coordinates = coordinates)

@app.route('/')
@as_json
def randomCoordinate():
	global lastCoordinate
	newCoordinate = Coordinate(str(fakerFactory.latitude()), str(fakerFactory.longitude()), datetime.utcnow())
	dt = abs(relativedelta(lastCoordinate.captured_at, newCoordinate.captured_at).seconds)
	if (dt >= 5):
		coordinate = newCoordinate
		lastCoordinate = newCoordinate
	else:
		coordinate = lastCoordinate

	#return json_response(coordinates = coordinate)
	return coordinate.__json__()

#@app.route('/pwd')
def pwd_test():
	pwd = 'abcd'
	pw_hash = fbcrypt.generate_password_hash(pwd).decode('utf-8')
	#app.logger.info(len(pw_hash))
	#app.logger.info(fbcrypt.check_password_hash(pw_hash, pwd))
	return pw_hash

#@app.route('/db')
def db_test():
	return str(Coordinate.query.all())

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()

#@app.route("/websocket")
def websocketTest():
	return render_template('websocket-test.html')
	
# #################### Android API V2 ##############################
# ready[(sender, receiver)], time[(sender, receiver)] GET (receiver: default android1)

# receivers: android1, ..., android99; web1, ..., web99

'''
Android/Web:
GET https://tracker.gobbi.info/connect?sender=arduino&receiver=android1
'''
@app.route('/connect')
def connect_arduino():
	global ready

	sender_arg = request.args.get('sender')
	receiver_arg = request.args.get('receiver')

	sender = Sender.query.filter(Sender.login == sender_arg).first()
	sender_exists = sender != None
	
	receiver = Receiver.query.filter(Receiver.login == receiver_arg).first()
	receiver_exists = receiver != None

	if sender_exists and receiver_exists:
		ready[(sender.login, receiver.login)] = True
		return  json_response(200)

	return json_response(400)

'''
Android/Web:
GET https://tracker.gobbi.info/disconnect?sender=arduino&receiver=android1
'''
@app.route('/disconnect')
def disconnect_arduino():
	global ready

	sender_arg = request.args.get('sender')
	receiver_arg = request.args.get('receiver')

	sender = Sender.query.filter(Sender.login == sender_arg).first()
	sender_exists = sender != None
	
	receiver = Receiver.query.filter(Receiver.login == receiver_arg).first()
	receiver_exists = receiver != None

	if sender_exists and receiver_exists:
		ready[(sender.login, receiver.login)] = False
		return  json_response(200)

	return json_response(400)


'''
Android/Web:
GET https://tracker.gobbi.info/coordinates?sender=arduino&receiver=android1
'''
@app.route('/coordinates')
def get_coordinates():
	global time
	#app.logger.info("0 /coordinates time map: " + str(time));
	sender_login = request.args.get('sender')
	receiver_login = request.args.get('receiver')		

	if (sender_login, receiver_login) in time:
		t0 = time[(sender_login, receiver_login)]
		now = datetime.utcnow()
		coordinates = Coordinate.query.filter(and_(Coordinate.login == sender_login, t0 < Coordinate.captured_at, Coordinate.captured_at <= now)).all()
		
		#app.logger.info("1 /coordinates time map: " + str(time));
		
		if coordinates:
			time[(sender_login, receiver_login)] = now

		#app.logger.info("2 /coordinates time map: " + str(time));

		return json_response(200, coordinates = coordinates)
	else:
		return json_response(400)


# map: time or ready
def contains_sender(_map, sender_login):
	for key in _map:
		if key[0] == sender_login:
			return True
	return False

# map: time or ready
def set_value_for_sender(_map, sender_login, value):
	at_least_one_sender_found = False
	for key in _map:
		if key[0] == sender_login:
			_map[key] = value
			at_least_one_sender_found = True
	return at_least_one_sender_found

def ready_pairs(ready, sender_login):
	pairs = []
	for key in ready:
		if key[0] == sender_login and ready[key]:
			pairs += [key]
	return pairs

def at_least_one_ready(ready, sender_login):
	for key in ready:
		if key[0] == sender_login and ready[key]:
			return True	
	return False

'''
Arduino
GET http://tracker.gobbi.info/c?l=arduino&p=jk+eUxwJ%5E%24aaDq2%5B
GET http://t.gobb.in/c?l=arduino&p=jk+eUxwJ%5E%24aaDq2%5B
'''
@app.route('/c')
def check_connection():
	global ready, time
	#app.logger.info("1 /check time map: " + str(time));
	login = request.args.get('l')
	password = request.args.get('p')

	_sender = Sender(login = login, password = password)

	sender = Sender.query.filter(Sender.login == _sender.login).first()
	sender_exists = sender != None

	if sender_exists and fbcrypt.check_password_hash(sender.password, _sender.password):
		if contains_sender(ready, sender.login):
			for a_pair in ready_pairs(ready, sender.login):
				time[a_pair] = datetime.utcnow()
			#app.logger.info("2 /check time map: " + str(time));
			return "OK"

	return "ERROR"

# GET http://tracker.gobbi.info/p?l=arduino&p=jk+eUxwJ%5E%24aaDq2%5B&lat=5320.12345&lng=5320.12345&t=20170127223431
# GET http://t.gobb.in/p?l=arduino&p=jk+eUxwJ%5E%24aaDq2%5B&lat=5320.12345&lng=5320.12345&t=20170127223431
"""
Arduino
The raw gps information from the serial is in the form: DDmm.mmmmm. e.g.If the raw information is: 5320.12345, then it is: 53 degress and 20.12345 minutes. In order to convert it to decimal coordinates you divide the minutes by 60. i.e. 20.12345 / 60 = .33539 Finally, the decimal result: 53.33539
"""
@app.route('/p')
def add_coordinate():
	global ready

	# database upto4digits6digits

	login = request.args.get('l')
	password = request.args.get('p')
	latitude = request.args.get('lat')
	longitude = request.args.get('lng')
	captured_at = request.args.get('t')

	try:
		_sender = Sender(login = login, password = password)
		coordinate = Coordinate(latitude = arduinoToGoogleCoordinate(latitude), longitude = arduinoToGoogleCoordinate(longitude),\
			captured_at = datetime.strptime(captured_at, "%Y%m%d%H%M%S"), received_at = datetime.utcnow(), login = _sender.login) #yyyymmddHHMMSS

		sender = Sender.query.filter(Sender.login == _sender.login).first()
		sender_exists = sender != None

		if sender_exists and fbcrypt.check_password_hash(sender.password, _sender.password):
			if contains_sender(ready, sender.login) and at_least_one_ready(ready, sender.login):
				db_session.add(coordinate)
				db_session.commit()
				db_session.flush()				
				return "OK " + str(coordinate.serial_number)
			else:
				return "ERROR"

	except (KeyError, TypeError, ValueError):
		return "ERROR"

	return "ERROR"

# ##################################################################
