from sqlalchemy import Column, String, CHAR, Date, TIMESTAMP, ForeignKey, BigInteger
from sqlalchemy.orm import relationship
from tracker.database import Base

class Receiver(Base):
	__tablename__ = 'receiver'
	login = Column(String(20), primary_key=True)
	email = Column(String(50), unique = True, nullable=False)
	password = Column(CHAR(60), nullable=False)
	login_sender = Column(String(20), ForeignKey('sender.login'), unique = False, nullable=False)
	sender = relationship("Sender", back_populates="receivers")	

	def __init__(self, login=None, email=None, password=None, login_sender=None):
		self.login = login
		self.email = email
		self.password = password
		self.login_sender = login_sender

	def __repr__(self):
		return '[Receiver {}]'.format(self.login)

class Sender(Base):
	__tablename__ = 'sender'
	login = Column(String(20), primary_key=True)
	email = Column(String(50), unique = True, nullable=False)
	password = Column(CHAR(60), nullable=False)
	receivers = relationship("Receiver", back_populates="sender")
	coordinates = relationship("Coordinate", back_populates="sender")

	def __init__(self, login=None, email=None, password=None):
		self.login = login
		self.email = email
		self.password = password

	def __repr__(self):
		return '[Sender {}]'.format(self.login) 

class Coordinate(Base):
	__tablename__ = 'coordinate'
	latitude = Column(String(10), nullable=False)
	longitude = Column(String(10), nullable=False)
	captured_at = Column(TIMESTAMP(6), nullable=False)
	received_at = Column(TIMESTAMP(6), nullable=True)
	login = Column(String(20), ForeignKey('sender.login'), nullable=False)
	sender = relationship("Sender", back_populates="coordinates")
	serial_number = Column(BigInteger, primary_key=True)

	def __init__(self, latitude=None, longitude=None, captured_at=None, received_at=None, login=None):
		self.login = login
		self.captured_at = captured_at
		self.longitude = longitude
		self.latitude = latitude
		self.received_at = received_at

	def __repr__(self):
		return '[Coordinate ({}, {}, {}, {})]'.format(self.latitude, self.longitude, self.captured_at, self.received_at)

	def __json__(self):
		return {
			'captured_at': self.captured_at,
			'received_at': self.received_at,
			'latitude': self.latitude,
			'longitude': self.longitude,
			'serial_number':self.serial_number
		} 