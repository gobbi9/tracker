from setuptools import setup, find_packages

setup(
    name='tracker',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
	    'flask==0.12',
	    'flask_json==0.3.0',
	    'faker==0.7.7',
	    'raven==5.32.0',
	    'blinker==1.4',
        'flask-bcrypt==0.7.1',
        'Flask-SQLAlchemy==2.1',
        'cymysql'
	]
)