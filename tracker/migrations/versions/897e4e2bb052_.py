"""Changes to coordinate

time -> captured_at
+ received_at

Revision ID: 897e4e2bb052
Revises: 7f4ee9a02ed4
Create Date: 2018-04-12 23:27:35.265246

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '897e4e2bb052'
down_revision = '7f4ee9a02ed4'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('coordinate', 'time', existing_type=mysql.TIMESTAMP(fsp=6), existing_server_default=sa.text('CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)'), existing_nullable=False, new_column_name='captured_at')
    op.add_column('coordinate', 
        sa.Column('received_at', 
            mysql.TIMESTAMP(fsp=6), 
            server_default=sa.text('CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)'), 
            nullable=True
        )
    )

def downgrade():
    op.alter_column('coordinate', 'captured_at', existing_type=mysql.TIMESTAMP(fsp=6), existing_server_default=sa.text('CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)'), existing_nullable=False, new_column_name='time')
    op.drop_column('coordinate', 'received_at')
