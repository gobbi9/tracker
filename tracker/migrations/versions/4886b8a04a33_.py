"""Multiple clients preparation, rename table

- Remove member_since
- client -> sender

Revision ID: 4886b8a04a33
Revises: 73053c4f7d71
Create Date: 2018-04-09 14:27:55.508392

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4886b8a04a33'
down_revision = '73053c4f7d71'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('client', 'member_since')
    op.rename_table('client', 'sender')
    

def downgrade():
    op.rename_table('sender', 'client')
    op.add_column('client', 'member_since')
    op.alter_column('client', 'member_since',
               existing_type=sa.DATE(),
               nullable=False)    
