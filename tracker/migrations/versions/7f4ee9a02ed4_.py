"""Create and populate table receiver

Revision ID: 7f4ee9a02ed4
Revises: 4886b8a04a33
Create Date: 2018-04-09 16:07:44.587614

"""
from alembic import op
import sqlalchemy as sa
from flask_bcrypt import Bcrypt
from sqlalchemy.sql import table
from sqlalchemy import String, CHAR, ForeignKey, Column

# revision identifiers, used by Alembic.
revision = '7f4ee9a02ed4'
down_revision = '4886b8a04a33'
branch_labels = None
depends_on = None

fbcrypt = Bcrypt()

range_devices = (1,100)

android_login = 'android'
android_pwd = fbcrypt.generate_password_hash('android').decode('utf-8')
web_login = 'web'
web_pwd = fbcrypt.generate_password_hash('web').decode('utf-8')


def upgrade():	
	receiver_table = op.create_table('receiver',
		Column('login', String(20), primary_key=True),
		Column('email', String(50), unique = True, nullable=False),
		Column('password', CHAR(60), nullable=False),
		Column('login_sender', String(20), ForeignKey('sender.login'), unique = False, nullable=False)
	)
	
	op.bulk_insert(receiver_table, [{
		'login': android_login + str(i), 
		'email': android_login + str(i) + '@test.test', 
		'password': android_pwd, 
		'login_sender': 'arduino'
	} for i in range(*range_devices)])
	
	op.bulk_insert(receiver_table, [{
		'login': web_login + str(i), 
		'email': web_login + str(i) + '@test.test', 
		'password': web_pwd, 
		'login_sender': 'arduino'
	} for i in range(*range_devices)])


def downgrade():
	op.drop_table('receiver')
