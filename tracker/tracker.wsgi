activate_this = '/root/git/tracker/tracker/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys, glob
directories = glob.glob('/root/git/tracker/tracker/venv/lib/python3.*/site-packages')
directories.sort()
directories.reverse()
trackerPath = directories[0]
if not trackerPath in sys.path:
    sys.path.insert(0, trackerPath)
sys.stdout = sys.stderr

from tracker import app as application