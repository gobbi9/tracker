## Rodando a aplicação no Ubuntu/Mint

1. [Instalar docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Instalar docker-compose](https://docs.docker.com/compose/install/)

## Iniciando a aplicação (branch dev)

> Só eu posso pushar pra `master` por motivos de segurança, você pode trabalhar na branch `dev`

```bash
git clone -b dev git@gitlab.com:gobbi9/tracker.git
# git clone -b dev https://gitlab.com/gobbi9/tracker.git
cd tracker
uid=`id -u` gid=`id -g` docker-compose up --build -d 
```

## Verificar se os serviços foram inicializados

```bash
✔  12:42 ~/git/tracker [ master {origin/master} | …6 ] $ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                NAMES
2faa1724eac9        tracker:latest      "/bin/sh -c 'flask r…"   5 seconds ago       Up 5 seconds        5000/tcp                                             tracker
d0a4128cc1bf        adminer             "entrypoint.sh docke…"   6 seconds ago       Up 3 seconds        8080/tcp                                             adminer
f4e64292fdd4        mysql               "docker-entrypoint.s…"   6 seconds ago       Up 5 seconds        3306/tcp                                             tracker-mysql
c37c917199a4        traefik:latest      "/traefik --web --we…"   6 seconds ago       Up 4 seconds        0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 8080/tcp   traefik
```

## Urls importantes

> Erros de certificados aparecerão pois o certificado local é *self-signed*, no Firefox basta adicionar esse site para lista de exceções e no Chrome ignorar o aviso.

- API: http://tracker.127.0.0.1.xip.io ou https://tracker.127.0.0.1.xip.io
- Adminer (Fazer consultas no BD): https://adminer.127.0.0.1.xip.io (se precisar acessar o banco de dados por outra ferramenta sem ser o Adminer, como MySQL Workbench, fale comigo)
  - System = MySQL
  - Server = tracker-mysql
  - Username = Password = root
  - Database = tracker
- Træfik (Load Balancer): https://traefik.127.0.0.1.xip.io

## IMPORTANTE!

### Live Reload Flask

A aplicação Flask possui Live Reload, logo basta editar e salvar um arquivo .py qualquer para que as mudanças sejam feitas, você pode acompanhar o log para ver o que está acontecendo e ver os prints também:

```bash
docker logs -f tracker
```

### xip.io

[xip.io](http://xip.io) é um servidor especial de dns, que junto com Traefik, permite que você associe domínios locais à serviços sem precisar rodar um servidor DNS na sua máquina, i.e. app.127.0.0.1.xip.io é redirecionado para o localhost pro serviço *app*. Dessa forma o desenvolvedor não precisa decorar em que porta cada serviço está rodando.

#### Fazer mudanças no docker-compose.yml

**Mudanças** no arquivo de configuração docker-compose.yml **NÃO** são atualizadas automaticamente, caso precise mexer nesse arquivo, execute após a edição:

```bash
docker-compose up --remove-orphans --build -d
```

Caso esse comando não tenha resolvido, execute:

```bash
docker-compose down
docker system prune -af
docker-compose up --remove-orphans --build -d
```

Caso ainda esteja com problemas fale comigo.

#### Cleanup

Por padrão os containers/serviços são reiniciados quando o sistema é ligado, para pará-los:

```bash
cd RAIZ_DO_PROJETO # pasta em que se encontra docker-compose.yml 
docker-compose down
```

Liberar o espaço em disco usado por imagens não utilizadas:

```bash
docker system prune -af
```

Remover volumes (dados inseridos no banco de dados local após a inicialização (que não estão em `db/20171024-tracker_dump.sql.gz`) serão removidos):

```bash
docker volume prune -f
```

